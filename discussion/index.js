
let favoriteMovie = {

	Title: "Harry Potter",
	Publisher: "Bloomsbury",
	Year: "1997",
	Director: "Chris Columbus"
};

console.log(favoriteMovie);

console.log(favoriteMovie.Title); //ACCESSING OBJECT PROPERTY
console.log(favoriteMovie.Publisher);

favoriteMovie.Year = "2000"; //UPDATE PROPERTY






//MINI ACTIVITY

favoriteMovie.Title = "Ang Probinsiyano";
favoriteMovie.Publisher = "FPJ Production";
favoriteMovie.Year="2001";

console.log(favoriteMovie);


let course = {

	title: "Philosophy 101",
	description: "Learn the value of life",
	price: 5000,
	isActive: true,
	instructors: ["Mr. johnson", "Mrs. Smith", "Mr.Francis"]

};
console.log(course);
console.log(course.instructors);
console.log(course.instructors[1]); //ACCESS ARRAY W/IN AN OBJECT (objectname.propertyname[index])


course.instructors.pop(); //ARRAY METHODS CAN BE USED
console.log(course);







//MINI ACTIVITY

//1.

course.instructors.push("Mr. McGee"); //TO ADD USE .push
console.log(course);

//2. 

let course1 = course.instructors.includes("Mr. johnson"); //to know if its true use .includes
console.log(course1);




//INSTRUCTION 1: create a function to be able to add new instructors to our object

function addNewInstructor(instructor){
	let isAdded = course.instructors.includes(instructor); 
	if(isAdded){
		console.log("Instructor is added");
    }else {
    	course.instructors.push(instructor);
    	console.log("Thank you. Instructor added.");

	}
}


addNewInstructor("Mr. Marco");
addNewInstructor("Mr. Smith");


console.log(course);







let instructor = {};
console.log(instructor);

instructor.name = "James johnson"; //CAN ASSIGN PROPERTY THAT DOES NOT YET EXIST
instructor.age = 56;
instructor.gender = "male";
instructor.department = "Humanities";
instructor.salary = 50000;
instructor.subject = ["Philosophy", "Humanities", "Logic"];

instructor.address = {

	street: "#1 Maginhawa St.",
	city: "Quezon City",
	country: "Philippines"
}

console.log(instructor);


console.log(instructor.address.street);




//CONSTRUCTOR FUNCTION

function superhero(name,superpower,powerlevel){

//name : valueOfParameterName
//superpower: valueOfParameterSuperpower
//powerlevel: valueOfParameterlevel

	this.name = name;
	this.superpower = superpower;
	this.powerlevel = powerlevel;
}


//CREATE AN OBJECT OUT OF SUPERHERO CONSTRUCTOR FUNCTION
//new keyword is added to allow us to create a new object out of our function

let superhero1 = new superhero("Saitama", "One Punch", 30000);
console.log(superhero1);




//MINI ACTIVITY

function laptop(name,brand,price){


this.name = name;
this.brand = brand;
this.price = price;

}


 let laptop1 = new laptop("Macbook air","Apple", 50000);
 console.log(laptop1);

 let laptop2 = new laptop("Dell inspiron", "Dell", 40000);
 console.log(laptop2);





// OBJECT METHODS
//are functions that are associated with an object
//function is a property of an object and that function belongs to the object.
//methods are tasks that an object can perform


		//methods are functions associated as a property of an object
		//they are anonymous functions we can invoke using the property of the object.


let person = {
	name: "Slim Shady",
	talk: function(){

	// console.log(this);
	console.log("Hi! my name is, what? my name is who? " + this.name);

	}
}
//instruction: method should be able to receive a single object

let person2 = {
	name: "Dr. Dre",
	greet: function(friend){ //methods are functions inside an object

		console.log("Good day, " + friend.name);

	}
}

person.talk();

person2.greet(person);





// CONSTRUCTOR BUILT-IN METHOD

function Dog(name,breed){

	this.name  = name;
	this.breed = breed;
	this.greet = function(friend){
		console.log("Bark! bark, " + friend.name);
	}
}

let dog1 = new Dog("Rocky", "Bulldog",);
console.log(dog1);

dog1.greet(person);
dog1.greet(person2);


let dog2 = new Dog("Blackie", "rottweiler");
console.log(dog2);

dog2.greet(person); //same with dog 1 because they were created from the same function