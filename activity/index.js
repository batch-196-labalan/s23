let trainer = {
	age: 10,
	friends: {
		hoen: ["May", "Max"],
		kanto: ["brocky", "misty"]
	},
	name: "Ash Ketchum",
	pokemon: ["Pikachu","Charizard", "Squirtle", "Bulbasur"]
	
};

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer.pokemon);


console.log("Result of talk method:");

trainer = {
	name: "Pikachu",
	talk: function(){

	
	console.log(name + " I choose you! " );

	}
}

trainer.talk();



// CONSTRUCTOR METHOD




function pokemon(Name,Level){


	this.Name  = Name;
	this.Level = Level;
	this.Health= Level*3;
	this.Attack = Level*1.5;

	this.tackle = function(pokemon){
		console.log(Name + " has tackled " + pokemon.Name);
		console.log(pokemon.Name + " has fainted");
	
   }
   this.faint = function(poHealth){  	
   		let monHealth = poHealth.Health - 1.5;
   		if(monHealth <= poHealth.Health){
   		console.log(this.Name+ "'s health is " + poHealth.Health);
   }
   }

}
	// this.Health = Health;
	// this.Attack = Attack;
	

let pokemon1 = new pokemon("Pikachu", 12);
console.log(pokemon1);

let pokemon2 = new pokemon("Geodude", 8);
console.log(pokemon2);


let pokemon3 = new pokemon("Mewtwo", 100);
console.log(pokemon3);


pokemon1.tackle(pokemon2);


pokemon2.faint(pokemon1);



// p1 = {
// 	name: "Pikachu",
//     tackle: function(pokemon){

// 		console.log(p1);

// 	}

// }

// p1.tackle();


// pokemon2.tackle(p1);


// p2 = {
// 	name: "Geodude",
//     tackle: function(pokemon){

// 		console.log(p2);

// 	}

// }

// p2.tackle();



// p3 = {
// 	name: "Mewtwo",
//     tackle: function(pokemon){

// 		console.log(p3);

// 	}

// }

// p3.tackle();




// pokemon1.tackle(p2);
// pokemon2.tackle(p3);

